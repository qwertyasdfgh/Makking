import { unprocessableEntity } from "remix-utils";

export async function getOAuthToken(code) {
  let data;
  try {
    data = await fetch("https://osu.ppy.sh/oauth/token", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        client_id: process.env.OSU_CLIENT_ID,
        client_secret: process.env.OSU_CLIENT_SECRET,
        code: code,
        grant_type: "authorization_code",
        redirect_uri: process.env.OSU_REDIRECT_URI,
      }),
    });
  } catch {
    throw new Error("bad code");
  }

  const json = await data.json();
  return json;
}

export async function getMe(token) {
  const data = await fetch("https://osu.ppy.sh/api/v2/me/osu", {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const json = data.json();
  return json;
}

export async function verifyParamAuthenticity(
  data,
  session,
  sessionKey = "csrf",
  urlKey = "state"
) {
  if (!session.has(sessionKey)) {
    throw unprocessableEntity({
      message: "Can't find CSRF token in session.",
    });
  }

  if (!data.searchParams.get(urlKey)) {
    throw unprocessableEntity({
      message: "Can't find CSRF token in body.",
    });
  }

  if (data.searchParams.get(urlKey) !== session.get(sessionKey)) {
    throw unprocessableEntity({
      message: "Can't verify CSRF token authenticity.",
    });
  }
}
