export function County({ county }) {
  return (
    <li
      className="mx-0 my-4 flex w-full max-w-3xl flex-shrink-0 flex-col space-y-4 overflow-auto 
              rounded border border-ctp-surface0 bg-gradient-to-t from-ctp-crust to-ctp-mantle 
              p-6 shadow-lg shadow-ctp-surface0 sm:mx-4"
      key={county.id}
    >
      <h1 className="text-center text-4xl tracking-tighter text-ctp-subtext0">
        {county.name}
      </h1>
      {county?.players.length > 0 ? (
        <table className="w-full text-left">
          <thead>
            <tr className="border-b border-ctp-surface2 text-ctp-subtext1">
              <th className="p-2">Place</th>
              <th className="p-2">Player</th>
              <th className="p-2">
                <a href="?toSort=rank&type=asc">Rank</a>
              </th>
              <th className="p-2">
                <a href="?toSort=score&type=desc">Score</a>
              </th>
              <th className="p-2">
                <a href="?toSort=SSranks&type=desc">SS ranks</a>
              </th>
              <th className="p-2">
                <a href="?toSort=Sranks&type=desc">S ranks</a>
              </th>
              <th className="p-2">
                <a href="?toSort=Aranks&type=desc">A ranks</a>
              </th>
            </tr>
          </thead>
          <tbody>
            {county.players.map((player, index) => (
              <tr
                key={player.id}
                className="divide-x-2 divide-ctp-surface0 border border-ctp-surface2 odd:bg-ctp-crust even:bg-ctp-mantle"
              >
                <td className="whitespace-nowrap p-2 font-bold text-ctp-subtext1">
                  {index + 1}.
                </td>
                <td className="whitespace-nowrap p-2 text-ctp-subtext1">
                  <a
                    className="text-ctp-subtext1 hover:text-ctp-text hover:underline"
                    href={`https://osu.ppy.sh/u/${player.playerName}`}
                    rel="noreferrer"
                    target="_blank"
                  >
                    {player.playerName}
                  </a>
                </td>
                <td className="whitespace-nowrap p-2 text-ctp-subtext1">
                  {player.rank ? `#${player.rank}` : "-"}
                </td>
                <td className="whitespace-nowrap p-2 tracking-wide text-ctp-subtext1">
                  {new Intl.NumberFormat().format(player.score)}
                </td>
                <td className="whitespace-nowrap p-2 text-ctp-subtext1">
                  {player.SSranks}
                </td>
                <td className="whitespace-nowrap p-2 text-ctp-subtext1">
                  {player.Sranks}
                </td>
                <td className="whitespace-nowrap p-2 text-ctp-subtext1">
                  {player.Aranks}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p className="text-center">no players here yet :(</p>
      )}
    </li>
  );
}
