import { createCookieSessionStorage } from "@remix-run/node";

const secret = process.env.COOKIE_SECRET;

const { getSession, commitSession, destroySession } =
  createCookieSessionStorage({
    cookie: {
      name: "oauth",
      maxAge: 86400,
      secrets: [secret],
      path: "/",
      secure: true,
      httpOnly: true,
      sameSite: "lax",
    },
  });

export { getSession, commitSession, destroySession };
