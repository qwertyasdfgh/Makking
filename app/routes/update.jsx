import { redirect } from "@remix-run/node";
import { getSession } from "~/sessions";
import { getMe } from "~/utils";
import { prisma } from "~/db.server";
import { verifyAuthenticityToken } from "remix-utils";

export async function action({ request }) {
  const session = await getSession(request.headers.get("Cookie"));
  try {
    await verifyAuthenticityToken(request, session);
  } catch {
    throw new Error("something went wrong");
  }

  if (!session.has("userId"))
    throw new Error("OAuth token not found in cookie");

  const me = await getMe(session.get("userId"));
  const selfData = await prisma.player.findUnique({
    where: {
      playerId: parseInt(me.id),
    },
  });

  if (!selfData)
    throw new Error("Please submit your county before trying to update");

  const updateUser = await prisma.player.update({
    where: {
      playerId: me.id,
    },
    data: {
      playerId: me.id,
      playerName: me.username,
      score: String(me.statistics.ranked_score),
      rank: me.statistics.global_rank
        ? parseInt(me.statistics.global_rank)
        : null,
      SSranks:
        parseInt(me.statistics.grade_counts.ss) +
        parseInt(me.statistics.grade_counts.ssh),
      Sranks:
        parseInt(me.statistics.grade_counts.s) +
        parseInt(me.statistics.grade_counts.sh),
      Aranks: parseInt(me.statistics.grade_counts.a),
    },
  });

  if (!updateUser) throw new Error("failed to update");

  return redirect("/");
}
