import { redirect } from "@remix-run/node";
import { getMe, getOAuthToken, verifyParamAuthenticity } from "~/utils";
import { Link } from "@remix-run/react";
import { getSession } from "~/sessions";
import { commitSession } from "~/sessions";

export async function loader({ request }) {
  const session = await getSession(request.headers.get("Cookie"));
  const url = new URL(request.url);

  if (url.searchParams.get("error")) return redirect("/");

  try {
    await verifyParamAuthenticity(url, session);
  } catch {
    throw new Error("state mismatch");
  }

  if (session.has("userId")) {
    return redirect("/");
  }

  const code = url.searchParams.get("code");
  if (!code) return redirect("/");

  const data = await getOAuthToken(code);
  if (data.error) {
    throw new Error("could not get access_token");
  }

  const me = await getMe(data.access_token);
  if (!["FI", "AX"].includes(me.country.code)) {
    throw new Error("your not finnish lil bro");
  }
  if (process.env.NODE_ENV === "production" && me.statistics.play_time < 180000)
    throw new Error("Minimum of 50 hours playtime required");

  session.set("userId", data.access_token);

  return redirect("/", {
    headers: {
      "Set-Cookie": await commitSession(session),
    },
  });
}
