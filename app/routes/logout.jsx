import { redirect } from "@remix-run/node";
import { verifyAuthenticityToken } from "remix-utils";
import { destroySession, getSession } from "~/sessions";

export async function action({ request }) {
  const session = await getSession(request.headers.get("Cookie"));

  try {
    await verifyAuthenticityToken(request, session);
  } catch {
    throw new Error("something went wrong");
  }

  return redirect("/", {
    headers: {
      "Set-Cookie": await destroySession(session),
    },
  });
}
