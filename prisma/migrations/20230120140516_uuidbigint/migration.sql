/*
  Warnings:

  - The primary key for the `Player` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to alter the column `score` on the `Player` table. The data in that column could be lost. The data in that column will be cast from `Int` to `BigInt`.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Player" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "playerId" INTEGER NOT NULL,
    "playerName" TEXT NOT NULL,
    "score" BIGINT,
    "rank" INTEGER,
    "SSranks" INTEGER,
    "Sranks" INTEGER,
    "Aranks" INTEGER,
    "countyId" INTEGER NOT NULL,
    CONSTRAINT "Player_countyId_fkey" FOREIGN KEY ("countyId") REFERENCES "County" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Player" ("Aranks", "SSranks", "Sranks", "countyId", "id", "playerId", "playerName", "rank", "score") SELECT "Aranks", "SSranks", "Sranks", "countyId", "id", "playerId", "playerName", "rank", "score" FROM "Player";
DROP TABLE "Player";
ALTER TABLE "new_Player" RENAME TO "Player";
CREATE UNIQUE INDEX "Player_playerId_key" ON "Player"("playerId");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
