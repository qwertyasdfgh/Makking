/*
  Warnings:

  - Added the required column `Aranks` to the `Player` table without a default value. This is not possible if the table is not empty.
  - Added the required column `SSranks` to the `Player` table without a default value. This is not possible if the table is not empty.
  - Added the required column `Sranks` to the `Player` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Player" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "playerName" TEXT NOT NULL,
    "rank" INTEGER NOT NULL,
    "SSranks" INTEGER NOT NULL,
    "Sranks" INTEGER NOT NULL,
    "Aranks" INTEGER NOT NULL,
    "countyId" INTEGER NOT NULL,
    CONSTRAINT "Player_countyId_fkey" FOREIGN KEY ("countyId") REFERENCES "County" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Player" ("countyId", "id", "playerName", "rank") SELECT "countyId", "id", "playerName", "rank" FROM "Player";
DROP TABLE "Player";
ALTER TABLE "new_Player" RENAME TO "Player";
CREATE UNIQUE INDEX "Player_playerName_key" ON "Player"("playerName");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
