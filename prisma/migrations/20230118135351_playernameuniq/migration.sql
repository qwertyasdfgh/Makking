/*
  Warnings:

  - A unique constraint covering the columns `[playerName]` on the table `Player` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Player_playerName_key" ON "Player"("playerName");
