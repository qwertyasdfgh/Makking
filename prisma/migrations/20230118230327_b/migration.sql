-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Player" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "playerName" TEXT NOT NULL,
    "rank" INTEGER,
    "SSranks" INTEGER,
    "Sranks" INTEGER,
    "Aranks" INTEGER,
    "countyId" INTEGER NOT NULL,
    CONSTRAINT "Player_countyId_fkey" FOREIGN KEY ("countyId") REFERENCES "County" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Player" ("Aranks", "SSranks", "Sranks", "countyId", "id", "playerName", "rank") SELECT "Aranks", "SSranks", "Sranks", "countyId", "id", "playerName", "rank" FROM "Player";
DROP TABLE "Player";
ALTER TABLE "new_Player" RENAME TO "Player";
CREATE UNIQUE INDEX "Player_playerName_key" ON "Player"("playerName");
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
